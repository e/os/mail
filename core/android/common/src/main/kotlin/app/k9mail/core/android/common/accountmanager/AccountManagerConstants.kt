/*
 * Copyright MURENA SAS 2023
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package app.k9mail.core.android.common.accountmanager

object AccountManagerConstants {
    const val EELO_ACCOUNT_TYPE = "e.foundation.webdav.eelo"
    const val GOOGLE_ACCOUNT_TYPE = "e.foundation.webdav.google"
    const val YAHOO_ACCOUNT_TYPE = "e.foundation.webdav.yahoo"
    const val ACCOUNT_EMAIL_ADDRESS_KEY = "email_address"
    const val MAIL_CONTENT_AUTHORITY = "foundation.e.mail.provider.AppContentProvider"
    const val AUTH_TOKEN_TYPE = "oauth2-access-token"
    const val KEY_AUTH_STATE = "auth_state"
    const val USERNAME_HINT = "userNameHint"

    const val OPEN_APP_PACKAGE_AFTER_AUTH = "open_app_package_after_auth"
    const val OPEN_APP_ACTIVITY_AFTER_AUTH = "open_app_activity_after_auth"

    const val IGNORE_ACCOUNT_SETUP = "ignore_account_setup"

    val OPENID_ACCOUNT_TYPES = listOf(EELO_ACCOUNT_TYPE, GOOGLE_ACCOUNT_TYPE, YAHOO_ACCOUNT_TYPE)
    val ALL_ACCOUNT_TYPES = listOf(EELO_ACCOUNT_TYPE, GOOGLE_ACCOUNT_TYPE, YAHOO_ACCOUNT_TYPE)

    val GOOGLE_DOMAIN_LIST = listOf(
        ".gmail.com",
        ".googlemail.com",
        ".google.com",
    )

    val YAHOO_DOMAIN_LIST = listOf(
        ".yahoo.com",
    )

    const val toOpenAfterAuthActivity = "com.fsck.k9.activity.MessageList"
}
