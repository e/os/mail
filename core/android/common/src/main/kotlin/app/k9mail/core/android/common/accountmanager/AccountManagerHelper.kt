/*
 * Copyright MURENA SAS 2023
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package app.k9mail.core.android.common.accountmanager

import android.accounts.AccountManager
import android.content.Context
import android.os.Bundle
import app.k9mail.core.android.common.findActivity

object AccountManagerHelper {

    fun startOpenIdOAuthFlow(context: Context, accountType: String, email: String?) {
        val activity = context.findActivity() ?: return

        val accountManager = AccountManager.get(activity.applicationContext)

        val options = Bundle()
        options.putString(AccountManagerConstants.OPEN_APP_PACKAGE_AFTER_AUTH, activity.packageName)
        options.putString(
            AccountManagerConstants.OPEN_APP_ACTIVITY_AFTER_AUTH,
            AccountManagerConstants.toOpenAfterAuthActivity,
        )
        options.putString(AccountManagerConstants.USERNAME_HINT, email)

        accountManager.addAccount(accountType, null, null, options, activity, null, null)
    }

    fun getOpenIdAccountTypeByHostName(hostname: String): String? {
        if (containedInDomain(AccountManagerConstants.GOOGLE_DOMAIN_LIST, hostname)) {
            return AccountManagerConstants.GOOGLE_ACCOUNT_TYPE
        } else if (containedInDomain(AccountManagerConstants.YAHOO_DOMAIN_LIST, hostname)) {
            return AccountManagerConstants.YAHOO_ACCOUNT_TYPE
        }

        return null
    }

    private fun containedInDomain(domainList: List<String>, hostname: String): Boolean {
        for (domain in domainList) {
            if (hostname.lowercase().endsWith(domain)) {
                return true
            }
        }

        return false
    }
}
