/*
 * Copyright ECORP SAS 2022
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.fsck.k9.setup

import android.content.Context
import app.k9mail.core.android.common.accountmanager.AccountManagerConstants
import com.fsck.k9.Account
import com.fsck.k9.mail.AuthType
import com.fsck.k9.mail.oauth.AuthStateStorage
import com.fsck.k9.preferences.AccountManager
import timber.log.Timber
import android.accounts.Account as OsAccount
import android.accounts.AccountManager as OsAccountManager

object EeloAccountHelper {

    /**
     * Update oAuthState for google accounts which is logged in using accountManager
     * @return is the update operation successful or not
     */
    fun updateOAuthState(context: Context, accountManager: AccountManager, account: Account?): Boolean {
        // check params
        if (account == null) {
            Timber.w("updating OAuthState failed, account is null")
            return false
        }

        // validation
        if (account.incomingServerSettings.authenticationType != AuthType.XOAUTH2) {
            Timber.w("updating oAuthState failed, not oauth2 authType")
            return false
        }

        val osAccountManager = OsAccountManager.get(context)

        var result = false
        AccountManagerConstants.OPENID_ACCOUNT_TYPES.forEach {
            val openIdAccount = retrieveOpenIdAccountFromAccountManager(osAccountManager, it, account.email)
            if (openIdAccount != null) {
                updateOAuthState(account, osAccountManager, openIdAccount, accountManager)
                result = true
                return@forEach
            }
        }

        return result
    }

    fun updateOAuthState(context: Context, authStateStorage: AuthStateStorage): Boolean {
        // check params
        if (authStateStorage.getEmail().isNullOrBlank()) {
            Timber.w("updating OAuthState failed, email is null")
            return false
        }

        val osAccountManager = OsAccountManager.get(context)

        var result = false
        AccountManagerConstants.OPENID_ACCOUNT_TYPES.forEach {
            val openIdAccount =
                retrieveOpenIdAccountFromAccountManager(osAccountManager, it, authStateStorage.getEmail())
            if (openIdAccount != null) {
                updateOAuthState(authStateStorage, openIdAccount, osAccountManager)
                result = true
                return@forEach
            }
        }

        return result
    }

    private fun updateOAuthState(
        account: Account,
        osAccountManager: android.accounts.AccountManager,
        openIdAccount: android.accounts.Account?,
        accountManager: AccountManager,
    ) {
        account.oAuthState = osAccountManager.getUserData(openIdAccount, AccountManagerConstants.KEY_AUTH_STATE)
        accountManager.saveAccount(account)
    }

    private fun updateOAuthState(
        authStateStorage: AuthStateStorage,
        openIdAccount: android.accounts.Account?,
        osAccountManager: android.accounts.AccountManager,
    ) {
        val authState = osAccountManager.getUserData(openIdAccount, AccountManagerConstants.KEY_AUTH_STATE)
        authStateStorage.updateAuthorizationState(authorizationState = authState)
    }

    // If token is updated by mail, also update the accountManager
    fun updateAccountInAccountManager(
        context: Context?,
        account: OsAccount?,
        authState: String?,
        accessToken: String?,
    ) {
        if (context == null || account == null || authState == null || accessToken == null) {
            Timber.w("updating account for accountManager failed, invalid param.")
            return
        }

        val accountManager = OsAccountManager.get(context)
        accountManager.setAuthToken(account, AccountManagerConstants.AUTH_TOKEN_TYPE, accessToken)
        accountManager.setUserData(account, AccountManagerConstants.KEY_AUTH_STATE, authState)
    }

    fun retrieveOpenIdAccountFromAccountManager(context: Context?, email: String?): OsAccount? {
        if (context == null) {
            Timber.w("retrieve google accounts from accountManager failed, null context.")
            return null
        }

        val accountManager = OsAccountManager.get(context)

        var resultAccount: OsAccount? = null

        AccountManagerConstants.OPENID_ACCOUNT_TYPES.forEach {
            resultAccount = retrieveOpenIdAccountFromAccountManager(accountManager, it, email)
            if (resultAccount != null) {
                return@forEach
            }
        }

        return resultAccount
    }

    private fun retrieveOpenIdAccountFromAccountManager(
        accountManager: OsAccountManager?,
        accountType: String,
        email: String?,
    ): OsAccount? {
        if (accountManager == null || email.isNullOrEmpty()) {
            Timber.w("retrieve $accountType account from accountManager failed, invalid param")
            return null
        }

        val openIdAccounts = accountManager.getAccountsByType(accountType)
        for (openIdAccount in openIdAccounts) {
            val emailId = accountManager.getUserData(openIdAccount, AccountManagerConstants.ACCOUNT_EMAIL_ADDRESS_KEY)
            if (!email.equals(emailId, ignoreCase = true)) {
                continue
            }

            val authState = accountManager.getUserData(openIdAccount, AccountManagerConstants.KEY_AUTH_STATE)
            if (authState.isNullOrEmpty()) {
                return null
            }

            return openIdAccount
        }

        return null
    }
}
