package com.fsck.k9.ui.identity

import com.fsck.k9.Identity

class IdentityFormatter {
    fun getDisplayName(identity: Identity): String {
        return identity.description ?: getEmailDisplayName(identity)
    }

    fun getEmailDisplayName(identity: Identity): String {
        val stringBuilder = StringBuilder()
        val containsName = identity.name != null && identity.name!!.trim().isNotEmpty()

        if (containsName) {
            stringBuilder.append(identity.name)
                .append(" ");
        }

        if (identity.email != null && identity.email!!.trim().isNotEmpty()) {
            if (containsName) {
                stringBuilder.append("<")
            }

            stringBuilder.append(identity.email)

            if (containsName) {
                stringBuilder.append(">");
            }
        }

        return stringBuilder.toString();
    }
}
