package com.fsck.k9.activity.setup.accountmanager;


import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;


@Root(name = "outgoingServer", strict = false)
public class EeloMailAutoConfigOutgoingServer extends EeloMailAutoConfigBaseServer {

    @Element
    private String useGlobalPreferredServer;

    public String getUseGlobalPreferredServer() {
        return useGlobalPreferredServer;
    }

    public void setUseGlobalPreferredServer(String useGlobalPreferredServer) {
        this.useGlobalPreferredServer = useGlobalPreferredServer;
    }
}

