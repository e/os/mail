package com.fsck.k9.ui.messagelist

import android.view.View
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.fsck.k9.ui.R

sealed class MessageListViewHolder(view: View) : ViewHolder(view)

class MessageViewHolder(view: View) : MessageListViewHolder(view) {
    var uniqueId: Long = -1L

    var mainView: LinearLayout = view.findViewById(R.id.main_view)
    val selected: ImageView = view.findViewById(R.id.selected)
    val subject: TextView = view.findViewById(R.id.subject)
    val displayName: TextView = view.findViewById(R.id.displayName)
    val preview: TextView = view.findViewById(R.id.preview)
    val date: TextView = view.findViewById(R.id.date)
    val chip: ImageView = view.findViewById(R.id.account_color_chip)
    val threadCount: TextView = view.findViewById(R.id.thread_count)
    val flagged: CheckBox = view.findViewById(R.id.star)
    val attachment: ImageView = view.findViewById(R.id.attachment)
    val status: ImageView = view.findViewById(R.id.status)
    val unreadMessageIndicator : View = view.findViewById(R.id.unread_message_indicator)
    val rightCheveron : View = view.findViewById(R.id.right_chevron)
    val endDivider : View = view.findViewById(R.id.end_divider)
}

class FooterViewHolder(view: View) : MessageListViewHolder(view) {
    val text: TextView = view.findViewById(R.id.main_text)
}
