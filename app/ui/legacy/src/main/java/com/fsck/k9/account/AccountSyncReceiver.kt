/*
 * Copyright ECORP SAS 2022
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.fsck.k9.account

import android.accounts.AccountManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.annotation.RequiresApi
import app.k9mail.core.android.common.accountmanager.AccountManagerConstants
import com.fsck.k9.Account
import com.fsck.k9.Preferences
import com.fsck.k9.activity.setup.accountmanager.EeloAccountCreator
import com.fsck.k9.controller.push.PushController
import com.fsck.k9.job.K9JobManager
import java.util.concurrent.Executors
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class AccountSyncReceiver : BroadcastReceiver(), KoinComponent {

    companion object {
        private const val ACTION_PREFIX = "foundation.e.accountmanager.account"

        private const val ACCOUNT_CREATION_ACTION = "$ACTION_PREFIX.create"
        private const val ACCOUNT_REMOVAL_ACTION = "android.accounts.action.ACCOUNT_REMOVED"

    }

    private val pushController: PushController by inject()
    private val preferences: Preferences by inject()
    private val accountRemover: BackgroundAccountRemover by inject()
    private val jobManager: K9JobManager by inject()

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent == null) {
            return
        }

        when (intent.action) {
            ACCOUNT_CREATION_ACTION -> createNewAccount(context)
            ACCOUNT_REMOVAL_ACTION -> removeAccount(intent)
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun createNewAccount(context: Context?) {
        pushController.init()
        context?.let {
            Executors.newSingleThreadExecutor().execute {
                EeloAccountCreator.loadAccountsFromAccountManager(
                    it.applicationContext,
                    preferences,
                    accountRemover,
                    jobManager,
                    null
                )
            }
        }
    }

    private fun removeAccount(intent: Intent) {
        val account = getAccount(intent) ?: return
        accountRemover.removeAccountAsync(account.uuid)
    }

    private fun getAccount(intent: Intent) : Account? {
        val accountType = intent.extras?.getString(AccountManager.KEY_ACCOUNT_TYPE)

        if (!AccountManagerConstants.ALL_ACCOUNT_TYPES.contains(accountType)) {
            return null
        }

        val account = intent.extras?.getString(AccountManager.KEY_ACCOUNT_NAME) ?: return null

        preferences.accounts.forEach {
            if (it.email == account) {
                return it
            }
        }

        return null
    }
}
