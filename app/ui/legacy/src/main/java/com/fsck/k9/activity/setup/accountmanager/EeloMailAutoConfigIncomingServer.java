package com.fsck.k9.activity.setup.accountmanager;


import org.simpleframework.xml.Root;


@Root(name = "incomingServer", strict = false)
public class EeloMailAutoConfigIncomingServer extends EeloMailAutoConfigBaseServer {
}
