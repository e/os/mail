/*
 * Copyright MURENA SAS 2023
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.fsck.k9.ui.messagelist

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.fsck.k9.Account
import com.fsck.k9.controller.MessageReference
import com.google.gson.Gson
import com.google.gson.TypeAdapter
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import java.io.IOException
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking
import timber.log.Timber

class EmailCache constructor(private val context: Context, private val gson: Gson) {
    companion object {
        private const val MAX_CACHE_SIZE = 20
        private const val preferenceDataStoreName = "emailCache"
        private val Context.emailCacheDataStore by preferencesDataStore(preferenceDataStoreName)
    }

    private val MAIL_LIST_KEY = stringPreferencesKey("mail_list")
    private var isCacheShown = false

    suspend fun getCachedMails(): List<MessageListItem>? {
        if (isCacheShown) {
            return null
        }

        return runBlocking {
            fetchCachedMail()
        }
    }

    suspend fun saveLatestMails(mailList: List<MessageListItem>) {
        if (isCacheShown) return

        isCacheShown = true
        val cachedMailsWithLatest = getLatestMails(mailList)

        val listType = object : TypeToken<List<MessageListItem>>() {}.type
        val mailListJson = gson.toJson(cachedMailsWithLatest, listType)
        context.emailCacheDataStore.edit {
            it[MAIL_LIST_KEY] = mailListJson
        }
        Timber.d("Saved latest mails in the cache")
    }

    suspend fun deleteMail(messages: List<MessageReference>) {
        isCacheShown = false
        val cachedMessages = fetchCachedMail()?.toMutableList()
        cachedMessages?.let {
            messages.forEach { messageRef ->
                cachedMessages.removeIf { it.messageUid == messageRef.uid }
                saveLatestMails(cachedMessages)
            }
        }
    }

    private suspend fun fetchCachedMail(): List<MessageListItem>? {
        val listType = object : TypeToken<List<MessageListItem>>() {}.type
        val mailListJson = context.emailCacheDataStore.data.map { it[MAIL_LIST_KEY] }.firstOrNull()
        Timber.d("Cached email data: $mailListJson")
        return gson.fromJson(mailListJson, listType)
    }

    private suspend fun getLatestMails(mailList: List<MessageListItem>): MutableList<MessageListItem> {
        var cachedMailsWithLatest = mutableListOf<MessageListItem>()
        cachedMailsWithLatest.addAll(mailList)
        fetchCachedMail()?.let {
            cachedMailsWithLatest.addAll(it)
        }

        cachedMailsWithLatest.sortedByDescending { it.messageDate }
        val lastIndex = if (cachedMailsWithLatest.size < MAX_CACHE_SIZE) cachedMailsWithLatest.size else MAX_CACHE_SIZE
        cachedMailsWithLatest = cachedMailsWithLatest.subList(0, lastIndex)
        return cachedMailsWithLatest
    }
}

internal class CharSequenceTypeAdapter : TypeAdapter<CharSequence?>() {
    @Throws(IOException::class)
    override fun write(writer: JsonWriter, value: CharSequence?) {
        if (value == null) {
            writer.nullValue()
        } else {
            writer.value(value.toString())
        }
    }

    @Throws(IOException::class)
    override fun read(reader: JsonReader): CharSequence? {
        return if (reader.peek() === JsonToken.NULL) {
            reader.skipValue()
            null
        } else {
            reader.nextString()
        }
    }
}
