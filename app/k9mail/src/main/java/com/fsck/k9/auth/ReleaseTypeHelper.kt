/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.fsck.k9.auth

import android.annotation.SuppressLint
import com.fsck.k9.auth.ReleaseType.Community
import com.fsck.k9.auth.ReleaseType.Official
import com.fsck.k9.auth.ReleaseType.Test
import com.fsck.k9.auth.ReleaseType.Unavailable
import com.fsck.k9.logging.Timber

object ReleaseTypeHelper {

    private const val KEY_SYSTEM_PROPERTY_RELEASE_TYPE = "ro.lineage.releasetype"

    fun getReleaseType(): ReleaseType {
        val property = getSystemProperty(KEY_SYSTEM_PROPERTY_RELEASE_TYPE)

        return when (property) {
            Community.value -> Community
            Official.value -> Official
            Test.value -> Test
            else -> Unavailable
        }
    }
}

enum class ReleaseType(val value: String) {
    Community("community"),
    Official("official"),
    Test("test"),
    Unavailable("")
}

@SuppressLint("PrivateApi")
private fun getSystemProperty(key: String): String {
    return try {
        Class.forName("android.os.SystemProperties")
            .getMethod("get", String::class.java)
            .invoke(null, key) as String
    } catch (e: Exception) {
        Timber.e("Unable to determine system property for $key", e)
        Unavailable.value
    }
}
