/*
 * Copyright (C) 2024 e Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package com.fsck.k9.auth

import com.fsck.k9.BuildConfig
import com.fsck.k9.auth.ReleaseType.Community
import com.fsck.k9.auth.ReleaseType.Official
import com.fsck.k9.auth.ReleaseType.Test
import com.fsck.k9.auth.ReleaseType.Unavailable

object MicrosoftRedirectUriGenerator {
    fun generate(): String {
        val releaseType = ReleaseTypeHelper.getReleaseType()
        val redirectUriSuffix = when (releaseType) {
            Community -> BuildConfig.MICROSOFT_REDIRECT_URI_SUFFIX_COMMUNITY
            Official -> BuildConfig.MICROSOFT_REDIRECT_URI_SUFFIX_OFFICIAL
            Test, Unavailable -> BuildConfig.MICROSOFT_REDIRECT_URI_SUFFIX_TEST
        }

        // For debug build (foundation.e.mail.debug), update the hash of your debug keystore in Microsoft Azure portal,
        // and based on that, set the redirectUriSuffix as it is a combination of both
        // the applicationId and signature hash of the keystore.

        return "msauth://${BuildConfig.APPLICATION_ID}/$redirectUriSuffix"
    }
}
