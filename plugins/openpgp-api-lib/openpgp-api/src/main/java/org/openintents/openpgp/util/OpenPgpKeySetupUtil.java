/*
 * Copyright ECORP SAS 2022
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.openintents.openpgp.util;


import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentSender;

import org.openintents.openpgp.OpenPgpApiManager;
import org.openintents.openpgp.OpenPgpApiManager.OpenPgpApiManagerCallback;
import org.openintents.openpgp.OpenPgpApiManager.OpenPgpProviderError;
import org.openintents.openpgp.OpenPgpApiManager.OpenPgpProviderState;
import org.openintents.openpgp.OpenPgpError;
import org.openintents.openpgp.util.OpenPgpApi.IOpenPgpCallback;
import timber.log.Timber;


public class OpenPgpKeySetupUtil implements OpenPgpApiManagerCallback {

    private static final int NO_KEY = 0;
    private static final int REQUEST_CODE_API_MANAGER = 6;
    private static final int REQUEST_CODE_KEY_PREFERENCE = 7;

    private final boolean showAutocryptHint;
    private final String userId;
    private final OpenPgpApiManager openPgpApiManager;

    private long keyId;
    private boolean pendingIntentRunImmediately;
    private Intent cachedActivityResultData;
    private PendingIntent pendingIntent;
    private OpenPgpKeySetupCallBack callBack;

    public OpenPgpKeySetupUtil(OpenPgpApiManager openPgpApiManager, String openPgpProvider, String userId, boolean showAutocryptHint) {
        this.openPgpApiManager = openPgpApiManager;
        this.openPgpApiManager.setOpenPgpProvider(openPgpProvider, this);
        this.userId = userId;
        this.showAutocryptHint = showAutocryptHint;
    }

    public void registerCallBack(OpenPgpKeySetupCallBack callBack) {
        this.callBack = callBack;
    }

    public void startSetup() {
        switch (openPgpApiManager.getOpenPgpProviderState()) {
            // The GET_SIGN_KEY action is special, in that it can be used as an implicit registration
            // to the API. Therefore, we can ignore the UI_REQUIRED here. If it comes up regardless,
            // it will also work as a regular pending intent.
            case UI_REQUIRED:
            case OK: {
                initializePendingIntent();
                break;
            }
            default: {
                openPgpApiManager.refreshConnection();
                break;
            }
        }
    }

    @Override
    public void onOpenPgpProviderStatusChanged() {
        if (openPgpApiManager.getOpenPgpProviderState() == OpenPgpProviderState.OK) {
            retrievePendingIntentInfo();
        } else {
            pendingIntent = null;
            pendingIntentRunImmediately = false;
            cachedActivityResultData = null;
        }
    }

    @Override
    public void onOpenPgpProviderError(OpenPgpProviderError error) {
        if (error == OpenPgpProviderError.ConnectionLost) {
            openPgpApiManager.refreshConnection();
        }
    }

    private void retrievePendingIntentInfo() {
        Intent data;

        if (cachedActivityResultData != null) {
            data = cachedActivityResultData;
            cachedActivityResultData = null;
        } else {
            data = new Intent();
        }

        retrievePendingIntentInfo(data);
    }

    private void retrievePendingIntentInfo(Intent data) {
        data.setAction(OpenPgpApi.ACTION_GET_SIGN_KEY_ID);
        data.putExtra(OpenPgpApi.EXTRA_USER_ID, userId);
        data.putExtra(OpenPgpApi.EXTRA_PRESELECT_KEY_ID, keyId);
        data.putExtra(OpenPgpApi.EXTRA_SHOW_AUTOCRYPT_HINT, showAutocryptHint);
        OpenPgpApi api = openPgpApiManager.getOpenPgpApi();
        api.executeApiAsync(data, null, null, openPgpCallback);
    }

    private final IOpenPgpCallback openPgpCallback = result -> {
        int resultCode = result.getIntExtra(OpenPgpApi.RESULT_CODE, OpenPgpApi.RESULT_CODE_ERROR);
        switch (resultCode) {
            case OpenPgpApi.RESULT_CODE_SUCCESS:
            case OpenPgpApi.RESULT_CODE_USER_INTERACTION_REQUIRED: {
                PendingIntent pendingIntent = result.getParcelableExtra(OpenPgpApi.RESULT_INTENT);

                if (result.hasExtra(OpenPgpApi.EXTRA_SIGN_KEY_ID)) {
                    long keyId = result.getLongExtra(OpenPgpApi.EXTRA_SIGN_KEY_ID, NO_KEY);

                    updateData(keyId, pendingIntent);
                } else {
                    updateData(pendingIntent);
                }

                break;
            }

            case OpenPgpApi.RESULT_CODE_ERROR: {
                OpenPgpError error = result.getParcelableExtra(OpenPgpApi.RESULT_ERROR);
                Timber.e("RESULT_CODE_ERROR: %s", error.getMessage());
                break;
            }
        }
    };

    /**
     * if pendingIntent present, then start it, else retrieve pendingIntent info by calling openPgpApi
     */
    private void initializePendingIntent() {
        if (pendingIntent != null) {
            startPendingIntent();
            return;
        }

        pendingIntentRunImmediately = true;
        retrievePendingIntentInfo();
    }

    private void startPendingIntent() {
        if (pendingIntent == null) {
            Timber.e("Tried to launch pending intent but didn't have any?");
            return;
        }

        try {
            if (callBack != null) {
                callBack.startPendingIntentForResult(pendingIntent, REQUEST_CODE_KEY_PREFERENCE);
            }
        } catch (IntentSender.SendIntentException e) {
            Timber.e(e,"Error launching pending intent");
        } finally {
            pendingIntent = null;
        }
    }

    private void updateData(PendingIntent pendingIntent) {
        this.pendingIntent = pendingIntent;
        maybeRunPendingIntentImmediately();
    }

    private void updateData(long keyId, PendingIntent pendingIntent) {
        setKeyId(keyId);
        this.pendingIntent = pendingIntent;

        maybeRunPendingIntentImmediately();
    }

    private void maybeRunPendingIntentImmediately() {
        if (!pendingIntentRunImmediately) {
            return;
        }

        pendingIntentRunImmediately = false;
        startPendingIntent();
    }

    private void setKeyId(long newValue) {
        if (newValue == 0L) {
            return;
        }

        keyId = newValue;

        if (callBack != null) {
            callBack.saveKeyId(keyId);
        }
    }

    public boolean handleOnActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_API_MANAGER:
                openPgpApiManager.onUserInteractionResult();
                return true;

            case REQUEST_CODE_KEY_PREFERENCE:
                if (resultCode == Activity.RESULT_OK) {
                    cachedActivityResultData = data;
                    // this might happen early in the lifecycle (e.g. before onResume). if the provider isn't connected
                    // here, apiRetrievePendingIntentAndKeyInfo() will be called as soon as it is.
                    OpenPgpProviderState openPgpProviderState = openPgpApiManager.getOpenPgpProviderState();

                    if (openPgpProviderState == OpenPgpProviderState.OK || openPgpProviderState == OpenPgpProviderState.UI_REQUIRED) {
                        retrievePendingIntentInfo();
                    }
                }
                return true;
        }
        return false;
    }


    public interface OpenPgpKeySetupCallBack {

        void startPendingIntentForResult(PendingIntent pendingIntent, int requestCode) throws IntentSender.SendIntentException;

        void saveKeyId(long key);
    }
}
